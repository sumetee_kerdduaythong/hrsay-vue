export default {
  hrsayTitle:
    "HR SAY เป็นช่องทางที่ดีที่สุดที่จะสื่อสารเมื่อบริษัทได้รับการรีวิว",
  hrsayDesc1:
    "ได้ข้อมูลจากอดีตพนักงาน / พนักงานปัจจุบันในแง่มุมที่เป็นข้อเท็จจริง และแสดงความรู้สึกในเชิงสร้างสรรค์",
  hrsayDesc2: "เปิดโอกาสให้ HR มีข้อมูลไปต่อยอดพัฒนาองค์กรให้เข้มแข็ง",
  hrsayDesc3: "เป็นช่องทางให้ HR นำเสนอข้อมูลต่างๆเพื่อดึงดูดผู้สมัคร",
  hrsayInstructionTitle:
    "กรุณาเขียนอธิบายนโยบายบริษัทที่ส่งเสริมพนักงานในด้าน ชีวิตดี งานดี เงินดี สังคมดี",
  hrsayInstructionDesc:
    "เราขอแนะนำให้คุณอ่านรีวิวจากพนักงานที่เคยร่วมงานจริงกับคุณเพื่อคุณจะได้เห็นประเด็นที่ต้องการเขียนอธิบายด้วยนโยบายของคุณ",
  clickToReview: "คลิกเพื่ออ่านรีวิว",
  goodLife: "ชีวิตดี",
  goodWork: "งานดี",
  goodMoney: "เงินดี",
  goodSocial: "สังคมดี",
  homepage: "Home",
  postjob: "Post Job",
  buyPackage: "Buy Package"
};
